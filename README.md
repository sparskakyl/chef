# chef

chef is the cool discord bot for elixi.re that manages user register approval.

it also cooks food sometimes

## usage

copy `config.example.yml` to `config.yml` and add your elixi.re admin uploader
token and Discord bot token.

install chef:

```sh
$ pip install git+https://gitlab.com/elixire/chef
```

then boot chef by launching the chef cli:

```sh
$ python -m chef.cli
```

you should install `uvloop` for faster asyncio -- it will be detected and
applied automatically by chef.
