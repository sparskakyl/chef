from lifesaver.bot import Cog


class Restrict(Cog):
    def __global_check(self, ctx):
        # disallow usage in guilds other than elixi.re, but allow dm commands
        if ctx.guild is not None and ctx.guild != ctx.bot.elixire:
            return

        # only let devs use the bot
        return ctx.bot.is_dev(ctx.author)


def setup(bot):
    bot.add_cog(Restrict(bot))
