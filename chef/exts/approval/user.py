from dataclasses import dataclass

from discord.utils import snowflake_time

from chef.constants import BAD_PROVIDERS

__all__ = ['User']


@dataclass
class User:
    user_id: int
    username: str
    email: str
    discord_tag: str

    @property
    def is_bad(self):
        provider = self.email.split('@')[-1]
        return provider in BAD_PROVIDERS

    @property
    def created_at(self):
        return snowflake_time(self.user_id)

    def __str__(self):
        return f'{self.username} (ID: `{self.user_id}`)'

    def __repr__(self):
        return f'<User user_id={self.user_id} username={self.username}>'

    def to_json(self):
        return {
            '_user': True,
            'user_id': self.user_id,
            'username': self.username,
            'email': self.email,
            'discord_tag': self.discord_tag,
        }
