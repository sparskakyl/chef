from lifesaver.bot.errors import MessageError

__all__ = ['ProcessingError']


class ProcessingError(MessageError):
    """An error raised during job creation and handling."""
