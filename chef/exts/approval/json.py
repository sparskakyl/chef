from json import JSONEncoder


class FlexibleJSONEncoder(JSONEncoder):
    def default(self, thing):
        if hasattr(thing, 'to_json'):
            return thing.to_json()
