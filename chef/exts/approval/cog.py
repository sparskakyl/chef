import asyncio
import logging
from uuid import uuid4 as uuid

import discord
from discord.utils import find, get
from lifesaver.bot import Cog, group
from lifesaver.utils.timing import Timer
from lifesaver.utils.formatting import pluralize
from lifesaver.bot.storage import AsyncJSONStorage
from .errors import ProcessingError
from .job import Job, JobState
from .json import FlexibleJSONEncoder
from .user import User

log = logging.getLogger(__name__)


class Approval(Cog):
    def __init__(self, bot, *args, **kwargs):
        super().__init__(bot, *args, **kwargs)

        self.storage = AsyncJSONStorage(
            'jobs.json',
            encoder=FlexibleJSONEncoder,
            object_hook=self.loader_hook,
            loop=bot.loop,
        )
        self.jobs = self.storage.get('jobs', [])
        self.headers = {
            'Authorization': self.bot.config.elixire['admin_token']
        }
        self.inflation = asyncio.Event()

        self.loop.create_task(self._inflate_jobs())

    async def save_jobs(self):
        log.debug('Saving jobs.')
        await self.storage.put('jobs', [job.to_json() for job in self.jobs])

    def loader_hook(self, dct):
        if '_job' in dct:
            # message and guild will be partial for now until _inflate_jobs
            message_id = dct['message']
            return Job(
                id=dct['id'], user=dct['user'], state=dct['state'],
                bot=self.bot, cog=self, guild=None, message=message_id,
            )
        elif '_jobstate' in dct:
            return getattr(JobState, dct['name'])
        elif '_user' in dct:
            return User(
                user_id=dct['user_id'], username=dct['username'],
                email=dct['email'], discord_tag=dct['discord_tag'],
            )
        return dct

    async def _inflate_jobs(self):
        await self.bot.wait_until_ready()

        if not self.jobs:
            log.debug('No jobs to inflate.')
            self.inflation.set()
            return

        with Timer() as timer:
            await self._inflate_all_jobs()

        n_jobs = pluralize(with_quantity=True, job=len(self.jobs))
        await self.user_registers.send(
            f'Job inflation took {timer}. {n_jobs} loaded.'
        )

        self.inflation.set()

    async def _inflate_all_jobs(self):
        """
        When loading jobs.json, we cannot fetch any messages during the loading
        process because of asyncio. The job of this method is to "inflate" any
        message IDs into the actual message objects by calling into Discord's
        API. If a message associated with a job could not be found from its ID,
        it will be deleted.
        """

        if not self.jobs:
            log.debug('No jobs to inflate.')
        else:
            log.debug('Inflating jobs.')

        jobs_to_delete = {}
        for index, job in enumerate(self.jobs):
            message_id = job.message

            if not isinstance(message_id, int):
                log.debug('Job already had a message, skipping: %s', job)
                continue

            try:
                fetched_message = await self.user_registers.get_message(message_id)
            except discord.HTTPException:
                log.warning('Failed to find message %d, queueing job for deletion.', message_id)
                jobs_to_delete.add(job)
                continue

            # replace the message id with the actual message
            self.jobs[index].message = fetched_message
            self.jobs[index].guild = self.bot.elixire

        log.debug('Finished job inflation, %d jobs are now in the queue.', len(self.jobs))

        if not jobs_to_delete:
            return

        for job in jobs_to_delete:
            log.debug('Deleting job #%d.', index)
            self.jobs.remove(job)

        await self.user_registers.send(
            f'Warning: {len(jobs_to_delete)} jobs were removed because their '
            'associated messages could not be found.'
        )

    @property
    def user_registers(self):
        return self.bot.get_channel(self.bot.config.channels['user_registers'])

    @property
    def messages(self):
        return self.bot.config.messages

    async def send_email(self, job):
        log.info('Sending email as requested by job: %s', job)

        elixire_user_id = job.user.user_id
        url = self.bot.config.elixire['domain'] + f'/api/admin/activate_email/{elixire_user_id}'

        async with self.session.post(url, headers=self.headers) as resp:
            packet = await resp.json()
            if not packet['success']:
                raise ProcessingError('Failed to send email.')

    @group(name='jobs', aliases=['job'], invoke_without_command=True)
    async def cmd_jobs(self, ctx):
        """Shows the number of jobs."""
        n_jobs = len(self.jobs)
        n_pending = sum(1 for job in self.jobs if job.state is JobState.PENDING)
        n_waiting = sum(1 for job in self.jobs if job.state is JobState.WAITING)

        content = """**Jobs in the queue: {}**

{} waiting for the user to join.
{} waiting for developer approval."""
        message = content.format(
            n_jobs,
            pluralize(with_indicative=True, with_quantity=True, job=n_pending),
            pluralize(with_indicative=True, with_quantity=True, job=n_waiting),
        )
        await ctx.send(message)

    @cmd_jobs.command(name='cancel')
    async def cmd_jobs_cancel(self, ctx, job: Job):
        """Cancels a job."""
        await job.cancel(ctx.author)
        self.jobs.remove(job)
        await ctx.send(f'{self.bot.approve_emoji} Job cancelled.')

    @cmd_jobs.command(name='delete', hidden=True)
    async def cmd_jobs_delete(self, ctx, job: Job):
        """
        Forcibly deletes a job.

        No messages are edited when this happens.
        """
        self.jobs.remove(job)
        await ctx.send(f'{self.bot.approve_emoji} Job deleted.')

    @cmd_jobs.command(name='approve')
    async def cmd_jobs_approve(self, ctx, job: Job):
        """Approves a job."""
        await job.finish(ctx.author, True)
        await self.on_job_finish(job)

    @cmd_jobs.command(name='deny')
    async def cmd_jobs_deny(self, ctx, job: Job):
        """Denies a job."""
        await job.finish(ctx.author, False)
        await self.on_job_finish(job)

    @cmd_jobs.command(name='update', hidden=True)
    async def cmd_jobs_update(self, ctx, job: Job):
        """Manually update a job."""
        ctx.bot.loop.create_task(job.manual_update())
        await ctx.ok(ctx.bot.approve_emoji)

    @cmd_jobs.command(name='list')
    async def cmd_jobs_list(self, ctx):
        """Shows a detailed listing of all jobs."""
        if not self.jobs:
            await ctx.send('There are no jobs.')
            return

        def job_formatter(job):
            return f'`{job.id}` for elixi.re user: {job.user} (state: {job.state.name.lower()})'

        await ctx.send('\n'.join(map(job_formatter, self.jobs)))

    async def on_job_finish(self, job):
        log.debug('Job %s has finished, removing.', job)
        self.jobs.remove(job)
        await self.save_jobs()

    async def create_job(self, embed, fields):
        if embed.title != 'user registration webhook':
            raise ProcessingError(f'Unexpected title: {embed.title}')

        def grab(name, converter=None):
            field = get(fields, name=name)
            if not field:
                raise ProcessingError(f'Failed to find required field for job creation: {name}')

            if converter:
                return converter(field.value)
            else:
                return field.value

        user = User(
            user_id=grab('userid', int),
            username=grab('user name'),
            discord_tag=grab('discord user'),
            email=grab('email'),
        )

        # send the initial message that will be used to externally track job
        # state
        message = await self.user_registers.send(
            self.messages['job_created']
        )

        job = Job(
            # job state
            id=str(uuid()),
            user=user,
            state=JobState.PENDING,

            # just some refs
            bot=self.bot,
            guild=self.bot.elixire,
            cog=self,
            message=message,
        )
        log.info('Created job: %s', job)

        self.jobs.append(job)

        log.debug('Doing initial manual update.')

        # why do we need to do this? well, jobs are only updated when users
        # join, but let's update NOW in case the user is here already.
        await job.manual_update()

    async def on_raw_reaction_add(self, payload):
        await self.inflation.wait()

        message_id = payload.message_id

        if (payload.channel_id != self.user_registers.id or
            payload.guild_id != self.bot.elixire.id):
            log.debug('Ignoring boring reaction event.')
            return

        member = self.bot.elixire.get_member(payload.user_id)
        if not member:
            log.debug('Member not found (ID: %d), leaving.', payload.user_id)
            return

        # ignore other emojis
        if payload.emoji.id not in [self.bot.approve_emoji.id, self.bot.deny_emoji.id]:
            log.debug('Ignoring invalid emoji.')
            return

        # block bots and non-devs
        if member.bot or not self.bot.is_dev(member):
            log.debug('Ignoring invalid user type.')
            return

        # locate the job for this event
        job = find(lambda job: job.message.id == message_id, self.jobs)
        if not job:
            log.debug('Failed to find job for raw reaction event (message ID: %d).', message_id)
            return

        is_approval = payload.emoji.id == self.bot.approve_emoji.id

        await job.finish(member, is_approval)
        await self.on_job_finish(job)

    async def on_member_join(self, member):
        await self.inflation.wait()

        if member.guild != self.bot.elixire:
            return

        log.debug('%s (ID: %d) has joined.', str(member), member.id)

        # fulfill any jobs that need to be fulfilled
        for job in self.jobs:
            if job.user.discord_tag == str(member):
                log.debug('Fulfilling job due to member join: %s', job)
                await job.fulfill(member)

    async def on_message(self, message):
        await self.inflation.wait()

        if message.channel != self.user_registers or not message.webhook_id:
            return

        log.debug('Job arrived from webhook %d!', message.webhook_id)

        if not message.embeds:
            log.debug('Bailing, no embeds.')
            return

        embed = message.embeds[0]
        fields = embed.fields

        try:
            log.info('Creating job for message %d.', message.id)
            await self.create_job(embed, fields)
            await self.save_jobs()
        except ProcessingError as error:
            await self.user_registers.send(f'Error while creating job: {error}')
