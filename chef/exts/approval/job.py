import logging
from dataclasses import dataclass
from enum import Enum, auto

import discord
from discord.ext import commands
from discord.utils import get, find
from lifesaver.bot import Bot, Cog
from lifesaver.utils.formatting import human_delta

from chef.utils import timestamp
from .user import User

__all__ = ['JobState', 'Job']
log = logging.getLogger(__name__)


class JobState(Enum):
    # user has registered on elixi.re, waiting for them to join
    PENDING = auto()

    # user has joined the guild, waiting for a decision by a dev
    WAITING = auto()

    def to_json(self):
        return {'name': self.name, 'value': self.value, '_jobstate': True}


@dataclass
class Job:
    # unique uuid for this job
    id: str

    # the elixi.re user that caused this job to be created
    user: User

    # the state that this job is in. waiting for user/user has joined
    state: JobState

    bot: Bot
    cog: Cog  # actually, the Approval cog but we can't hint because import recursion
    guild: discord.Guild

    # message in #user-registers that handles this job
    message: discord.Message = None

    def __eq__(self, other):
        return other.id == self.id

    def to_json(self):
        return {
            'id': self.id,
            'user': self.user.to_json(),
            'state': self.state.to_json(),
            # bot, cog
            'guild': self.guild.id,
            'message': self.message.id,
            '_job': True,
        }

    @property
    def _reactions(self):
        return [self.bot.approve_emoji, self.bot.deny_emoji]

    @classmethod
    async def convert(cls, ctx, uuid):
        job = get(ctx.cog.jobs, id=uuid)
        if not job:
            raise commands.BadArgument('Job not found.')
        return job

    async def _clear_reactions(self):
        try:
            await self.message.clear_reactions()
        except discord.HTTPException:
            pass

    async def stale(self):
        self.state = JobState.PENDING
        await self.cog.save_jobs()
        await self.message.edit(content=self.cog.messages['job_pending'], embed=None)

    async def cancel(self, canceller):
        message = '{message} by {canceller} (ID: `{canceller.id}`).'
        await self._clear_reactions()
        await self.message.edit(content=message.format(
            message=self.cog.messages['job_cancelled'],
            canceller=canceller,
        ), embed=None)

    async def finish(self, executor: discord.User, outcome: bool):
        await self._clear_reactions()

        log.info('%s: Outcome was %s.', self.id, outcome)

        # edit job state tracking message to reflect that a decision was reached
        await self.message.edit(content=self.cog.messages['job_finished'], embed=None)

        outcome_string = 'APPROVED' if outcome else 'DENIED'
        channel = self.cog.user_registers

        # send a confirmation message just for logging purposes
        emoji = self.bot.approve_emoji if outcome else self.bot.deny_emoji

        message = (
            "{emoji} {discord_user}'s entry into elixi.re was **{outcome}** "
            "by {executor} (ID: `{executor.id}`) just now.\n\nTimestamp: {timestamp}"
        )

        await channel.send(
            message.format(
                emoji=emoji,
                discord_user=self.user.discord_tag,
                outcome=outcome_string,
                executor=executor,
                timestamp=timestamp()
            )
        )

        if not outcome:
            return

        # send the email for approval
        async with channel.typing():
            try:
                log.debug('%s: Requesting an email send to cog.', self.id)
                await self.cog.send_email(self)
            except Exception as error:
                await channel.send(
                    f'{self.bot.deny_emoji} Failed to send email. ({type(error).__name__}: {error})'
                )
            else:
                await channel.send(
                    f'{self.bot.approve_emoji} Email sent.'
                )

    async def fulfill(self, user):
        self.state = JobState.WAITING
        await self.cog.save_jobs()

        # update message
        embed = discord.Embed()
        embed.color = discord.Color.green()
        embed.description = self.cog.messages['job_fulfilled']

        if self.user.is_bad:
            embed.color = discord.Color.red()
            embed.description += f'\n\n\N{WARNING SIGN} **User uses a bad e-mail provider, be careful!**'

        embed.add_field(
            name='Discord',
            value=(f'{user} (ID: `{user.id}`)'
                   f'\nJoined Discord {human_delta(user.created_at)} ago')
        )
        embed.add_field(
            name='elixi.re',
            value=(f'{self.user.username} (ID: `{self.user.user_id}`)'
                   f'\nRegistered {human_delta(self.user.created_at)} ago')
        )
        embed.add_field(name='Joined server', value=f'{user.joined_at} UTC')

        await self.message.edit(content='', embed=embed)

        # add reactions
        for reaction in self._reactions:
            await self.message.add_reaction(reaction)

    async def manual_update(self):
        log.debug('%s: Updating.', self.id)

        # grab any offline members if we big
        if self.guild.large:
            await self.bot.request_offline_members(self.guild)

        # attempt to find the user
        discord_user = find(lambda u: str(u) == self.user.discord_tag, self.guild.members)

        if discord_user is None:
            # zzz...
            await self.stale()
        else:
            # we good, user has joined
            await self.fulfill(discord_user)

        await self.cog.save_jobs()

    def __repr__(self):
        return f'<Job id={self.id} user={self.user!r} state={self.state!r}>'
