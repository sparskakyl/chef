import datetime

__all__ = ['timestamp']


def timestamp() -> str:
    return datetime.datetime.utcnow().strftime('%m/%d/%Y %H:%M:%S') + ' UTC'
