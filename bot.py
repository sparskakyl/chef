import discord
from discord.ext import commands
from lifesaver.bot import Bot


class Chef(Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ignored_errors = [commands.CheckFailure, commands.NotOwner]

    def is_dev(self, member):
        if isinstance(member, discord.User):
            # not in a guild, so let's fetch the member from elixi.re's guild
            member = self.elixire.get_member(member.id)
            if member is None:
                return False

        allowed_roles = self.config.restrict['allowed_roles']
        return any(role.id in allowed_roles for role in member.roles)

    @property
    def approve_emoji(self):
        return self.get_emoji(self.config.emojis['approve'])

    @property
    def deny_emoji(self):
        return self.get_emoji(self.config.emojis['deny'])

    @property
    def elixire(self):
        return self.get_guild(self.config.guilds['elixire'])
